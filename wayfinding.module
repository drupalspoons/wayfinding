<?php

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implements hook_entity_base_field_info().
 *
 * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
 *
 * @return array|void
 */
function wayfinding_entity_base_field_info(EntityTypeInterface $entity_type) {
  if (!in_array($entity_type->id(), Drupal::service('wayfinding.entity_types')->allDisabledIds())) {
    /** @var \Drupal\wayfinding\EntityUpdate $service */
    $service = Drupal::service('wayfinding.entity_update');
    return [
      'wayfinding' => $service->fieldDefinition(),
    ];
  }
}

/**
 * Implements hook_entity_insert().
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 */
function wayfinding_entity_insert(EntityInterface $entity) {
  Drupal::service('wayfinding.events')->update($entity);
}

/**
 * Implements hook_entity_update().
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 */
function wayfinding_entity_update(EntityInterface $entity) {
  Drupal::service('wayfinding.events')->update($entity);
}

/**
 * Implements hook_entity_delete().
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 */
function wayfinding_entity_delete(EntityInterface $entity) {
  Drupal::service('wayfinding.events')->update($entity);
}

/**
 * Implements hook_form_alter().
 *
 * @param array $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * @param $form_id
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function wayfinding_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  if ($form_id === 'media_wayfinding_edit_form') {
    $form['name']['#access'] = FALSE;
    $form['field_perspective']['#access'] = FALSE;
    $form['field_destination']['#access'] = FALSE;
    $form['wayfinding']['#access'] = FALSE;
  }
  elseif ($form_id === 'media_wayfinding_add_form') {
    $form['name']['#access'] = FALSE;
    $form['field_perspective']['#access'] = FALSE;
    $form['field_destination']['#access'] = FALSE;
    $form['wayfinding']['#access'] = FALSE;
    foreach (['name', 'perspective', 'destination_type', 'destination_id'] as $item) {
      if (!isset($_GET[$item])) {
        $form['actions']['#access'] = FALSE;
        Drupal::messenger()->addWarning(t('You should not add wayfinding media directly, go to <a href="@url">Way finding</a> instead', [
          '@url' => Url::fromRoute('wayfinding.collection')->toString(),
        ]));
        return;
      }
    }
    $form['name']['widget'][0]['value']['#default_value'] = $_GET['name'];
    $form['field_perspective']['widget'][0]['value']['#default_value'] = $_GET['perspective'];
    $form['field_destination']['widget'][0]['target_type']['#default_value'] = $_GET['destination_type'];
    $form['field_destination']['widget'][0]['target_id']['#default_value'] = Drupal::entityTypeManager()->getStorage($_GET['destination_type'])->load($_GET['destination_id']);
  }
  elseif  (isset($form['wayfinding'])) {
    $config = Drupal::configFactory()->get('wayfinding.settings');
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $form_state->getBuildInfo()['callback_object']->getEntity();
    $id = implode(' ', [$entity->getEntityTypeId(), $entity->bundle()]);
    $isDestination = !empty($config->get('types')['destinations'][$id]);
    $isSource = !empty($config->get('types')['sources'][$entity->getEntityTypeId()]);
    if (!$isDestination && !$isSource) {
      $form['wayfinding']['#access'] = FALSE;
    }
    elseif (isset($form['advanced'])) {
      $form['wayfinding']['widget'][0]['#group'] = 'advanced';
      $form['wayfinding']['widget'][0]['#open'] = FALSE;
      $form['wayfinding']['widget'][0]['#weight'] = 99;
    }
  }
}

/**
 * Implements hook_inline_entity_form_entity_form_alter().
 *
 * @param array $entity_form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function wayfinding_inline_entity_form_entity_form_alter(&$entity_form, FormStateInterface $form_state) {
  if ($entity_form['#entity_type'] === 'wayfinding') {
    $entity_form['label']['#states']['disabled']['input[name="wayfinding[0][inline_entity_form][auto_label][value]"]'] = ['checked' => TRUE];
    foreach (['auto_label', 'label', 'geolocation', 'svgid'] as $item) {
      $entity_form[$item]['#states']['invisible']['input[name="wayfinding[0][inline_entity_form][status][value]"]'] = ['checked' => FALSE];
    }
    if ($isDestination = method_exists($form_state->getBuildInfo()['callback_object'], 'getEntity')) {
      $config = Drupal::configFactory()->get('wayfinding.settings');
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $form_state->getBuildInfo()['callback_object']->getEntity();
      $id = implode(' ', [$entity->getEntityTypeId(), $entity->bundle()]);
      $isDestination = !empty($config->get('types')['destinations'][$id]);
    }
    if (!$isDestination) {
      $entity_form['geolocation']['#access'] = FALSE;
      $entity_form['svgid']['#access'] = FALSE;
    }
  }
}

/**
 * Implements hook_theme().
 */
function wayfinding_theme() {
  return [
    'wayfinding' => [
      'variables' => [
        'view' => NULL,
        'blocks' => [],
        'did' => 0,
        'eid' => '',
      ],
    ],
    'views_view_wayfinding_wrapper' => [
        'variables' => [
          'view_array' => [],
          'view' => NULL,
          'rows' => [],
          'header' => [],
          'footer' => [],
          'empty' => [],
          'feed_icons' => [],
          'title' => '',
          'attachment_before' => [],
          'attachment_after' => [],
          'popupdestination' => 'popup',
          'popupcontent' => '',
        ],
      'template' => 'views-view-wayfinding-wrapper',
    ],
  ];
}

/**
 * Prepares variables for views wayfinding master templates.
 *
 * Default template: views-view-wayfinding-wrapper.html.twig.
 *
 * @param array $variables
 */
function wayfinding_preprocess_views_view_wayfinding_wrapper(&$variables) {
  /** @var \Drupal\views\ViewExecutable $view */
  $view = $variables['view'];
  if (empty($view->args[1])) {
    $perspective = $view->args[0] ?? 0;
    $variables['bgimg'] = Drupal::service('wayfinding.query')->getRenderedMedia($perspective, 'user', 1, TRUE);
    $variables['pinimg'] = Drupal::service('wayfinding.query')->getRenderedPin();
  }
  $variables['popupdestination'] = Drupal::service('wayfinding.query')->getPopupDestination();
  $variables['popupcontent'] = Drupal::service('wayfinding.query')->getPopupContent();
  $variables['#attached']['library'][] = 'wayfinding/general';

  // Sort attachments
  foreach (['before', 'after'] as $type) {
    if (isset($variables['attachment_' . $type])) {
      $list = $variables['attachment_' . $type];
      $variables['attachment_' . $type] = [];
      foreach ($list as $item) {
        $display = $view->storage->getDisplay($item['#display_id']);
        $variables['attachment_' . $type][$display['position']] = $item;
      }
      ksort($variables['attachment_' . $type]);
    }
  }
}
