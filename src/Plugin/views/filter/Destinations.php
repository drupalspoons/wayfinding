<?php

namespace Drupal\wayfinding\Plugin\views\filter;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter handler for wayfinding destinations.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("wayfinding_destinations")
 */
class Destinations extends FilterPluginBase {

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Config\Config $config
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Config $config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @noinspection NullPointerExceptionInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('wayfinding.settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {}

  /**
   * {@inheritdoc}
   */
  protected function operatorForm(&$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function canExpose() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $table = $this->ensureMyTable();
    $types = [];
    foreach ($this->config->get('types')['destinations'] as $key => $value) {
      if (!empty($value)) {
        $types[] = $key;
      }

    }
    $types = implode("','", $types);
    $snippet = "$table.parent_entity_type_bundle IN ('$types')";
    $this->query->addWhereExpression($this->options['group'], $snippet);
  }

}
