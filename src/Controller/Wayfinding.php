<?php

namespace Drupal\wayfinding\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Render\HtmlResponseAttachmentsProcessor;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\wayfinding\Query;
use Endroid\QrCode\QrCode;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the Device controller.
 */
class Wayfinding implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Tbd.
   *
   * @var \Drupal\wayfinding\Query
   */
  protected $query;

  /**
   * Tbd.
   *
   * @var \Drupal\Core\Render\HtmlResponseAttachmentsProcessor
   */
  protected $attachmentProcessor;

  /**
   * Tbd.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Tbd.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Device constructor.
   *
   * @param \Drupal\wayfinding\Query $query
   *   Tbd.
   * @param \Drupal\Core\Render\HtmlResponseAttachmentsProcessor $attachment_processor
   *   Tbd.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Tbd.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Tbd.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(Query $query, HtmlResponseAttachmentsProcessor $attachment_processor, ConfigFactoryInterface $config_factory, RequestStack $request_stack, EntityTypeManagerInterface $entity_type_manager) {
    $this->query = $query;
    $this->attachmentProcessor = $attachment_processor;
    $this->config = $config_factory->get('wayfinding.settings');
    $this->request = $request_stack->getCurrentRequest();
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Wayfinding {
    return new static(
      $container->get('wayfinding.query'),
      $container->get('html_response.attachments_processor'),
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Tbd.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Tbd.
   */
  public function access(): AccessResultInterface {
    return AccessResult::allowed();
  }

  /**
   * Tbd.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   Tbd.
   */
  public function request(): HtmlResponse {
    return $this->deliver(0);
  }

  /**
   * Tbd.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Tbd.
   */
  public function popup(): AjaxResponse {
    $output = $this->query->build(TRUE, 0);
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#wayfinding-popup .wayfinding-popup-content', $output));
    return $response;
  }

  /**
   * Tbd.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Tbd.
   */
  public function widgets(): AjaxResponse {
    // TODO: Sanitize data before processing.
    $data = json_decode($this->request->query->get('data'), TRUE);
    $response = new AjaxResponse();

    // Build route widgets.
    if (!empty($data['position']['lat']) && !empty($data['position']['lng']) && $this->config->get('enable qr code')) {
      $routeWidgets = '';
      foreach ($data['destinations'] as $destination) {
        if (!empty($destination['lat']) && !empty($destination['lng'])) {
          $url = 'https://www.google.com/maps/dir/' . $data['position']['lat'] . ',' . $data['position']['lng'] . '/' . $destination['lat'] . ',' . $destination['lng'];
          $qrCode = new QrCode($url);
          $routeWidgets .= '<a href="' . $url . '"><img  alt="Open route in Google Maps" src="data:image/png;base64,' . base64_encode($qrCode->writeString()) . '"></a>';
        }
      }
      if (!empty($routeWidgets)) {
        $response->addCommand(new HtmlCommand('#wayfinding .widgets .widget.route', $routeWidgets));
        $response->addCommand(new InvokeCommand('#wayfinding .widgets .widget.route', 'addClass', ['show']));
      }
    }

    return $response;
  }

  /**
   * Tbd.
   *
   * @param float $perspective
   *   Tbd.
   * @param int $lat
   *   Tbd.
   * @param int $lng
   *   Tbd.
   * @param int $did
   *   Tbd.
   * @param string $eid
   *   Tbd.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   Tbd.
   */
  protected function deliver($perspective, $lat = 0, $lng = 0, $did = 0, $eid = 'undefined'): HtmlResponse {
    $html_response = new HtmlResponse();
    $html_response->setContent($this->query->build(FALSE, $perspective, $lat, $lng, $did, $eid));
    // $html_response->getCacheableMetadata()->setCacheMaxAge(0);
    // $html_response->getCacheableMetadata()->setCacheTags([]);
    // $html_response->getCacheableMetadata()->setCacheContexts([]);
    return $this->attachmentProcessor->processAttachments($html_response);
  }

}
