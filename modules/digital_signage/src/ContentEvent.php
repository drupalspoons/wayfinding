<?php

namespace Drupal\wayfinding_digital_signage;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\digital_signage_computed_content\ComputedContentInterface;
use Drupal\digital_signage_computed_content\Entity\ComputedContent;
use Drupal\digital_signage_framework\Entity\ContentSetting;
use Drupal\digital_signage_framework\DeviceInterface;

/**
 * Content event service.
 */
class ContentEvent {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs an Entity update service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return bool
   */
  protected function saveEntity(EntityInterface $entity): bool {
    try {
      $entity->save();
      return TRUE;
    }
    catch (EntityStorageException $e) {
      // TODO: Log this exception.
    }
    return FALSE;
  }

  /**
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *
   * @return string
   */
  protected function getComputedContentTitle(DeviceInterface $device): string {
    return 'Wayfinding for device ' . $device->label();
  }

  /**
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *
   * @return \Drupal\digital_signage_computed_content\ComputedContentInterface
   */
  protected function getComputedContentEntity(DeviceInterface $device): ComputedContentInterface {
    try {
      $entities = $this->entityTypeManager->getStorage('digital_signage_content_setting')
        ->loadByProperties([
          'parent_entity__target_type' => 'digsig_computed_content',
          'parent_entity_bundle' => 'wayfinding',
          'devices' => $device->id(),
        ]);
    }
    catch (InvalidPluginDefinitionException $e) {
      // TODO: Log this exception.
    }
    catch (PluginNotFoundException $e) {
      // TODO: Log this exception.
    }
    /** @var \Drupal\digital_signage_framework\ContentSettingInterface $settings */
    if (empty($entities)) {
      $settings = ContentSetting::create(['devices' => [$device]]);
      $this->saveEntity($settings);
    }
    else {
      $settings = reset($entities);
    }

    /** @var \Drupal\digital_signage_computed_content\ComputedContentInterface $content */
    if ($reverse_entity = $settings->getReverseEntity()) {
      $content = ComputedContent::load($reverse_entity['target_id']);
    }
    else {
      $content = ComputedContent::create([
        'bundle' => 'wayfinding',
        'digital_signage' => $settings,
        'title' => $this->getComputedContentTitle($device),
      ]);
      $this->saveEntity($content);
    }
    return $content;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function insert(EntityInterface $entity): void {
    if (!($entity instanceof DeviceInterface)) {
      // Only deal with digital signage device entities.
      return;
    }
    /** @var \Drupal\digital_signage_framework\DeviceInterface $device */
    $device = $entity;
    $this->getComputedContentEntity($device);
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function update(EntityInterface $entity): void {
    if (!($entity instanceof DeviceInterface)) {
      // Only deal with digital signage device entities.
      return;
    }
    /** @var \Drupal\digital_signage_framework\DeviceInterface $device */
    $device = $entity;
    $content = $this->getComputedContentEntity($device);
    $title = $this->getComputedContentTitle($device);
    if ($title !== $content->getTitle()) {
      $content->setTitle($title);
      $this->saveEntity($content);
    }
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function delete(EntityInterface $entity): void {
    if (!($entity instanceof DeviceInterface)) {
      // Only deal with digital signage device entities.
      return;
    }
    /** @var \Drupal\digital_signage_framework\DeviceInterface $device */
    $device = $entity;
    $content = $this->getComputedContentEntity($device);
    try {
      $content->delete();
    }
    catch (EntityStorageException $e) {
      // TODO: Log this exception.
    }
  }

}
