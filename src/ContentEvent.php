<?php

namespace Drupal\wayfinding;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\wayfinding\Entity\Wayfinding;

/**
 * Content event service.
 */
class ContentEvent {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs an Entity update service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function update(EntityInterface $entity) {
    if (!($entity instanceof ContentEntityInterface)) {
      // Only deal with content entities.
      return;
    }

    /** @var ContentEntityInterface $entity */
    if (!$entity->hasField('wayfinding')) {
      // Only deal with entities that have the wayfinding field;
      return;
    }

    // Load the settings entity.
    $target = $entity->get('wayfinding')->getValue();
    if (!isset($target[0]['target_id'])) {
      // Might be missing in some circumstances.
      return;
    }
    /** @var \Drupal\wayfinding\WayfindingInterface $settings */
    $settings = Wayfinding::load($target[0]['target_id']);
    $changed = FALSE;

    // Store reverse reference.
    if (!$settings->getReverseEntity()) {
      $settings
        ->setReverseEntity($entity)
        ->setReverseEntityTypeAndBundle(implode(' ', [$entity->getEntityTypeId(), $entity->bundle()]));
      $changed = TRUE;
    }

    // Disable wayfinding ifreverse entity is disabled.
    if ($entity->hasField('status') && !$entity->get('status')->value && $settings->isEnabled()) {
      $settings->setStatus(FALSE);
      $changed = TRUE;
    }

    if ((empty($settings->getLabel()) || $settings->isAutoLabel()) && $settings->getLabel() !== $entity->label()) {
      $settings->setLabel($entity->label());
      $changed = TRUE;
    }

    if ($changed) {
      $settings->save();
    }
  }

}
