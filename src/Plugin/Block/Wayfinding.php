<?php

namespace Drupal\wayfinding\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\wayfinding\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Wayfinding' Block.
 *
 * @Block(
 *   id = "wayfinding",
 *   admin_label = @Translation("Wayfinding"),
 * )
 */
class Wayfinding extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\wayfinding\Query
   */
  protected $query;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Query $query) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->query = $query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('wayfinding.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->query->build(TRUE, 0);
  }

}
