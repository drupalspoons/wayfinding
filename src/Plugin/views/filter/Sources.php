<?php

namespace Drupal\wayfinding\Plugin\views\filter;

/**
 * Filter handler for wayfinding sources.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("wayfinding_sources")
 */
class Sources extends Destinations {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $table = $this->ensureMyTable();
    $types = [];
    foreach ($this->config->get('types')['sources'] as $key => $value) {
      if (!empty($value)) {
        $types[] = $key;
      }

    }
    $types = implode("','", $types);
    $snippet = "$table.parent_entity__target_type IN ('$types')";
    $this->query->addWhereExpression($this->options['group'], $snippet);
  }

}
