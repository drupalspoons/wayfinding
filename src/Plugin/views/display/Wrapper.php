<?php

namespace Drupal\wayfinding\Plugin\views\display;

use Drupal\views\Plugin\views\display\DisplayPluginBase;

/**
 * The plugin that handles a wayfinding wrapper.
 *
 * @ingroup views_display_plugins
 *
 * @ViewsDisplay(
 *   id = "wayfinding_wrapper",
 *   title = @Translation("Wayfinding Wrapper"),
 *   help = @Translation("Display the view as a wayfinding wrapper."),
 *   theme = "views_view_wayfinding_wrapper",
 *   register_theme = FALSE,
 *   uses_hook_block = FALSE,
 *   contextual_links_locations = {""},
 *   admin = @Translation("Wayfinding Wrapper")
 * )
 */
class Wrapper extends DisplayPluginBase {

  protected $usesAJAX = FALSE;
  protected $usesPager = FALSE;
  protected $usesMore = FALSE;
  protected $usesAttachments = TRUE;
  protected $usesAreas = FALSE;

  public function usesExposed() {
    return FALSE;
  }

}
